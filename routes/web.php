<?php

use GuzzleHttp\Middleware;
use Illuminate\Routing\RouteGroup;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

Route::group(['prefix' => 'admin', 'middleware' => 'auth:admin'], function () {
    Route::get('/dashboard' , 'AdminController@index')->name('admin.dashboard');
    Route::post('/add-state' , 'AdminController@stateInvestor')->name('state.create');
    Route::get('/delete-investor/{id}' , 'AdminController@deleteInvestor')->name('investor.delete');
    Route::post('/add-return/{id}' , 'AdminController@addReturn')->name('return.add');
    Route::post('/add-admin' , 'AdminController@addAdmin')->name('admin.create');
});
Route::group(['prefix' => 'admin'], function () {
    Route::get('/login' , 'SessionController@showAdminForm')->name('admin.login');
    Route::post('/login' , 'SessionController@adminLogin')->name('admin.login.submit');
    Route::get('/logout' , 'SessionController@adminLogout')->name('admin.logout');
});

Route::get('/' , 'PagesController@index')->name('home');
Route::get('/contact' , 'PagesController@contact')->name('contact');
Route::get('/concept' , 'PagesController@concept')->name('concept');
Route::get('/justification' , 'PagesController@justification')->name('justification');
Route::get('/activities' , 'PagesController@activities')->name('activities');
Route::get('/register' , 'SessionController@showForm')->name('register');
Route::post('/register' , 'SessionController@register')->name('user.create');


