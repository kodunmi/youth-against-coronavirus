<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class WelcomeNotification extends Notification implements ShouldQueue
{
    use Queueable;

    public $user;
    public $content;
    public $result;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($user)
    {
        $this->user = $user;
        $this->content= 'Welcome to youth action against corona virus <br> Youth Against Coronavirus is a coalition designed to galvanize coordinated youth response and participation in national efforts to manage and halt further spread of the novel Coronavirus (COVID-19) pandemic in Nigeria. <br> The coalition is to ensure that Nigerian youths, NGOs and young professionals are mobilized as stakeholders to actively contribute knowledge, expertise and resources to support governmental efforts at eradicating the virus both at national and state government levels in a coordinated and measurable way.';
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
        ->view('mails.welcome-notification',[
            'name' => $this->user->firstname,
            'content' => $this->content,
        ]);
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
