<?php

namespace App\Http\Controllers;


use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Notifications\WelcomeNotification;

class SessionController extends Controller
{
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
        $this->middleware('guest:admin')->except('adminLogout');
    }

    public function showForm(){
        return view('frontend.pages.register');
    }

    public function showAdminForm(){
        return view('admin.pages.login');
    }


    public function register(Request $request){
        $request->validate([
            'firstname' => 'required',
            'lastname' => 'required',
            'email' => 'required|email|unique:users,email',
            'state' => 'required',
            'gender' => 'required',
            'social_status' => 'required'

        ]);

        $user = User::create([
            'firstname' => $request->firstname,
            'lastname' => $request->lastname,
            'email' => $request->email,
            'state' => $request->state,
            'gender' => $request->gender,
            'social_status' => $request->social_status,
        ]);

        $user->notify(new WelcomeNotification($user));


        return back()->with([
            'message' => 'You have successfully pledged with youths against corona.',
            'type' => 'success'
        ]);
    }
    public function adminLogin(Request $request){
        $request->validate([
            'email' => ['required', 'email'],
            'password' => ['required'],
        ]);
        $remember = $request->has('remember')? true : false;
        if(Auth::guard('admin')->attempt(['email' => $request->email, 'password' => $request->password], $remember)){
            return redirect()->route('admin.dashboard');
        }else{
            return back()->withErrors(['error' => 'We do not have your credentials']);
        }
    }

    public function adminLogout(Request $request){
        Auth::guard('admin')->logout();
        $request->session()->flush();
        $request->session()->regenerate();

        return redirect()->route('admin.login');
    }

    public function logout(Request $request){
        Auth::logout();
        $request->session()->flush();
        $request->session()->regenerate();

        return redirect()->route('login');
    }

}
