<?php

namespace App\Http\Controllers;

use App\Admin;
use App\State;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class AdminController extends Controller
{
    public function index(){

       return view('admin.pages.dashboard', [
           'users' => User::all(),
           'admins' => Admin::all(),
           'states' => State::all(),
       ]);
    }

    public function stateInvestor(Request $request){
        $request->validate([
            'name' => 'required',
        ]);

        $state = new State;
        $state->name = $request->name;
        $state->save();

        return back()->with([
            'message' => 'state created successfully',
            'type' => 'success'
        ]);
    }

    public function deleteInvestor($id){
        User::find($id)->delete();

        return back()->with([
            'message' => 'investor has been deleted successfully',
            'type' => 'success'
        ]);
    }

    public function addReturn(Request $request, $id){
        $request->validate([
            'amount' => 'required|numeric',
        ]);

        // $return = new ROI([
        //     'amount' => $request->amount
        // ]);

        // User::find($id)->rois()->save($return);
        // return back()->with([
        //     'message' => 'new return added',
        //     'type' => 'success'
        // ]);

    }
    public function addAdmin(Request $request){

        $request->validate([
            'firstname' => 'required',
            'lastname' => 'required',
            'email' => 'required|email',
            'password' => 'required|min:8|confirmed'
        ]);

        $admin = new Admin;
        $admin->firstname = $request->firstname;
        $admin->lastname = $request->lastname;
        $admin->email = $request->email;
        $admin->password = Hash::make($request->password);
        $admin->save();

        return back()->with([
            'message' => 'new admin registered successfully',
            'type' => 'success'
        ]);

    }

    public function deleteAdmin($id){
        if(Admin::all()->count() == 1){
            return back()->with([
                'message' => 'You cannot delete the last admin',
                'type' => 'danger'
            ]);
        }

        Admin::find($id)->delete();

        return back()->with([
            'message' => 'Admin has been deleted successfully',
            'type' => 'success'
        ]);
    }
}
