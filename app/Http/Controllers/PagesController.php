<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;

class PagesController extends Controller
{
    public function index(){
        return view('frontend.pages.index',[
            'users' => User::all()->count()
        ]);
    }
    public function contact(){
        return view('frontend.pages.contact');
    }

    public function dashboard(){
        return view('frontend.pages.dashboard');
    }
    public function activities(){
        return view('frontend.pages.activities');
    }
    public function concept(){
        return view('frontend.pages.concept');
    }
    public function justification(){
        return view('frontend.pages.justification');
    }
}
