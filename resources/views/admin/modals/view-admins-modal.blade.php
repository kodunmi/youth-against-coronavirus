<div id="view-admin" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="my-modal-title" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="my-modal-title">Add Return</h5>
                <button class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body text-center">
                <div class="table-responsive">
                    <table class="table">
                        <thead>
                            <tr>
                                <th> Profile </th>
                                <th> Delete </th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($admins as $admin)
                            <tr>
                                <td>
                                    <img src={{ "https://ui-avatars.com/api/?name=".$admin->lastname."+".$admin->firstname."&background=9a55ff&color=fff&size=50" }} alt="image" class="mr-2">
                                    {{ $admin->lastname.' '.$admin->firstname }}
                                </td>
                                <td> 
                                    <a href="{{route('admin.delete' ,['id' => $admin->id])}}">
                                        <span class="page-title-icon bg-gradient-danger text-white mr-2">                                    
                                            <i type="button" class="mdi mdi-delete-forever user-table-icon"></i>
                                        </span>
                                    </a>
                                </td>
                            </tr>
                            @include('admin.modals.delete-admin-modal',['admin' => $admin])
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="modal-footer">
                <button class="btn btn-light" data-dismiss="modal" aria-label="Close">Cancel</button>
            </div>

        </div>
    </div>
</div>