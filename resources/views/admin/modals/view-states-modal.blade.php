<div id="view{{$state->id}}" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="my-modal-title" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="my-modal-title">Investers Details</h5>
                <button class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body text-center">
                <ul class="list-ticked">
                    <li>Name: {{$state->name}}</li>
                </ul>
                <br><br>
                <h3>Investors Returns</h3>
                <ul class="list-ticked">
                    @foreach ($state->reports as $report)
                        <div class="card">
                            <div class="card-body">
                                <h5 class="card-title">Report</h5>
                                <p class="text-muted mb-2">$report->date</p>
                                <p class="card-text">$repor->report</p>
                            </div>
                        </div>
                    @endforeach
                </ul>
            </div>
            <div class="modal-footer">
                <button class="btn btn-light" data-dismiss="modal" aria-label="Close">Cancel</button>
            </div>
        </div>
    </div>
</div>
