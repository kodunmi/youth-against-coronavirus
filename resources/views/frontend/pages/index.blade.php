@extends('frontend.layout._master') @section('content')
<!-- Hero section -->
<section class="hero-section">
    <div class="container">
        <div class="row">
            <div class="col-md-6 hero-text">
                <h2>Youth <span>Against</span> <br>Corona Virus</h2>
                <h4>The coalition is to ensure that Nigerian youths, NGOs and young professionals are mobilized as stakeholders to actively contribute knowledge, expertise and resources to support governmental efforts at eradicating the virus both at national
                    and state government levels in a coordinated and measurable way.</h4>
                <a href="{{route('concept')}}" class="d-block readmore mt-2">Readmore</a>
                <a href="{{ route('register')}}" class="site-btn sb-gradients mt-5">Pledge</a>

            </div>
            <div class="col-md-6">
                <img src="/frontend/img/Coronavirus.png" class="laptop-image" alt="">
            </div>
        </div>
    </div>
</section>
<!-- Hero section end -->
<!-- About section -->
<section class="about-section spad" id="process">
    <div class="container">
        <div class="row">
            <div class="col-lg-6 offset-lg-6 about-text" id="pledge">
                <h2>Pledge</h2>
                <p>I pledge to Nigeria my country To join the fight to eradicate Coronavirus from Nigeria And to take seriously all safety measures against the virus I take responsibility for protecting myself and others by pledging to</p>
                <ul class="pledge-list">
                    <li>Wash my hands regularly with soap and water,</li>
                    <li>Practice social distancing and avoid large gatherings,</li>
                    <li>Stay at home and avoid unnecessary outings.</li>
                </ul>
                <p class="mt-5">I pledge to Nigeria my country</p>
                <p>That I will self-isolate and report to the necessary authorities if I show any COVID-19 related symptoms</p>
                <p>I pledge not to join in spreading unconfirmed information through social media or verbally, that may misinform and increase panic</p>
                <p>I will also be supportive to those in need at this challenging time</p>
                <p><strong>We shall eradicate Coronavirus from Nigeria in record time, so help us God!</strong></p>
                <a href="{{ route('register')}}" class="site-btn sb-gradients mt-5">Pledge</a>
            </div>
        </div>
        <div class="about-img">
            <img src="/frontend/img/about-img.png" alt="">
        </div>
    </div>
</section>
<!-- About section end -->
<!-- Features section -->
<section class="features-section spad gradient-bg" id="obj">
    <div class="container text-white">
        <div class="row">
            <div class="col-md-6" id="action">
                <h2>Take Action</h2>
                <h3>Take action in the following areas</h3>
                <ul class="mt-3 action-list">
                    <li>Promote the YAC Pledge among youth</li>
                    <li>Support YAC National and State activities</li>
                    <li>Initiate a project or solution against Coronavirus</li>
                    <li>Share relevant information through YAC Platforms</li>
                </ul>
            </div>
            <div class="col-md-6">
                <h2>Volunteer</h2>
                <h3>Volunteers are needed in the following areas</h3>
                <ul class="mt-3 action-list">
                    <li>Program Officers</li>
                    <li>Graphics designer</li>
                    <li>Infographics creator</li>
                    <li>Video editor/Animator</li>
                    <li>Reporters</li>
                    <li>Studio presenters/ News Analysts</li>
                </ul>
                <p class="mt-2"><strong>To volunteer, kindly send an email with your area of interest, CV and
                    Photo Passport to …</strong></p>
            </div>
        </div>
    </div>
</section>
<!-- Features section end -->


<!-- Process section -->
<section class="process-section spad">
    <div class="container">
        <div class="section-title text-center">
            <h2>Justification and Activities</h2>
            <p>All about justification and activities of youth against coronavirus</p>
        </div>
        <div class="row">
            <div class="col-md-6 process">
                <div class="process-step">
                    <figure class="process-icon">
                        <img class="h-54" src="/frontend/img/balance-scale.png" alt="#">
                    </figure>
                    <h4>Justification</h4>
                    <p>The novel deadly COVID-19 pandemic virus has entered Nigeria and threatens our national existence if allowed to spread at the exponential....</p>
                    <a href="{{route('justification')}}" class="readmore">Readmore</a>
                </div>
            </div>
            <div class="col-md-6 process">
                <div class="process-step">
                    <figure class="process-icon">
                        <img class="h-54" src="/frontend/img/intellectual.png" alt="#">
                    </figure>
                    <h4>Proposed Activities</h4>
                    <p>
                        <ul>
                            <li>Produce and disseminate youth centric information about Coronavirus through social media channels;</li>
                        </ul>
                        <p>And more</p>
                    </p>
                    <a href="{{route('activities')}}" class="readmore">Readmore</a>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- Process section end -->


<!-- Fact section -->
<section class="fact-section gradient-bg">
    <div class="container">
        <div class="row">
            <div class="col-sm-6 col-md-6 col-lg-6">
                <div class="fact">
                    <h2>{{ $users }}</h2>
                    <p>Registered <br> Members</p>
                    <i class="ti-basketball"></i>
                </div>
            </div>
            <div class="col-sm-6 col-md-6 col-lg-6">
                <div class="fact">
                    <h2>240</h2>
                    <p>Countries <br> included</p>
                    <i class="ti-users"></i>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- Fact section end -->

<!-- Review section -->
<section class="review-section spad">
    <div class="container">
        <div class="row">
            <div class="col-lg-8 offset-lg-2">
                <h2 class="text-center">YAC REVIEW</h2>
                <img src="/frontend/img/quote.png" alt="" class="quote mb-5">
                <div class="review-text-slider owl-carousel">
                    <div class="review-text">
                        <h3 class="mb-2">Kaduna State</h3>
                        <span class="text-muted mb-5">2nd of November</span>
                        <div class="mt-5">
                            <p>"Bitcoin is exciting because it shows how cheap it can be. Bitcoin is better than currency in that you don’t have to be physically in the same place and, of course, for large transactions, currency can get pretty inconvenient.”</p>
                        </div>
                        <a href="{{route('concept')}}" class="d-block readmore mt-2">Readmore</a>
                    </div>
                    <div class="review-text">
                        <p>"Bitcoin is exciting because it shows how cheap it can be. Bitcoin is better than currency in that you don’t have to be physically in the same place and, of course, for large transactions, currency can get pretty inconvenient.”</p>
                    </div>
                    <div class="review-text">
                        <p>Bitcoin is exciting because it shows how cheap it can be. Bitcoin is better than currency in that you don’t have to be physically in the same place and, of course, for large transactions, currency can get pretty inconvenient.”</p>
                    </div>
                </div>

                <h3 class="mt-5">View All Review</h3 class="mt-5">
            </div>
            {{--
            <div class="col-lg-4 pr-0 pull-3">
                <div class="review-meta-slider owl-carousel pt-5">
                    <div class="author-meta">
                        <div class="author-avatar set-bg" data-setbg="/frontend/img/review/1.jpg"></div>
                        <div class="author-name">
                            <h4>Aaron Ballance</h4>
                            <p>Ceo Bitcoin</p>
                        </div>
                    </div>
                    <div class="author-meta">
                        <div class="author-avatar set-bg" data-setbg="/frontend/img/review/2.jpg"></div>
                        <div class="author-name">
                            <h4>Jackson Nash</h4>
                            <p>Head of Design</p>
                        </div>
                    </div>
                    <div class="author-meta">
                        <div class="author-avatar set-bg" data-setbg="/frontend/img/review/3.jpg"></div>
                        <div class="author-name">
                            <h4>Katy Abrams</h4>
                            <p>Product Manager</p>
                        </div>
                    </div>
                </div>
            </div> --}}
        </div>
    </div>
</section>
<!-- Review section end -->


<!-- Newsletter section -->
<section class="features-section pd-2 gradient-bg">
</section>
<!-- Newsletter section end -->



<!-- Blog section -->
<section class="blog-section spad" id="blog">
    <div class="container">
        <div class="section-title text-center">
            <h2>Latest News</h2>
            <p>Bitcoin is the simplest way to exchange money at very low cost.</p>
        </div>
        <div class="row">
            <!-- blog item -->
            <div class="col-md-4">
                <div class="blog-item">
                    <figure class="blog-thumb">
                        <img src="/frontend/img/blog/1.jpg" alt="">
                    </figure>
                    <div class="blog-text">
                        <div class="post-date">03 jan 2018</div>
                        <h4 class="blog-title"><a href="">Coinbase to Reopen the GDAX Bitcoin Cash-Euro Order Book</a>
                        </h4>
                        <div class="post-meta">
                            <a href=""><span>by</span> Admin</a>
                            <a href=""><i class="fa fa-heart-o"></i> 234 Likes</a>
                            <a href=""><i class="fa fa-comments-o"></i> 08 comments</a>
                        </div>
                    </div>
                </div>
            </div>
            <!-- blog item -->
            <div class="col-md-4">
                <div class="blog-item">
                    <figure class="blog-thumb">
                        <img src="/frontend/img/blog/2.jpg" alt="">
                    </figure>
                    <div class="blog-text">
                        <div class="post-date">28 dec 2018</div>
                        <h4 class="blog-title"><a href="">Blockchain Rolls Out Trading Feature for 22 States in the
                                U.S</a></h4>
                        <div class="post-meta">
                            <a href=""><span>by</span> Admin</a>
                            <a href=""><i class="fa fa-heart-o"></i> 234 Likes</a>
                            <a href=""><i class="fa fa-comments-o"></i> 08 comments</a>
                        </div>
                    </div>
                </div>
            </div>
            <!-- blog item -->
            <div class="col-md-4">
                <div class="blog-item">
                    <div class="blog-text">
                        <a class="twitter-timeline" data-height="370" href="https://twitter.com/NCDCgov?ref_src=twsrc%5Etfw">Tweets by NCDCgov</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- Blog section end -->
@endsection