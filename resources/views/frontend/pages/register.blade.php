@extends('frontend.layout._master')
@section('content')
<!-- Page info section -->
	<section class="page-info-section">
		<div class="container">
			<h2>Take Action</h2>
			<div class="site-beradcamb">
				<a href="{{ route('home')}}">Home</a>
				<span><i class="fa fa-angle-right"></i> Take Action</span>
			</div>
		</div>
	</section>
	<!-- Page info end -->



	<!-- Contact section -->
	<section class="contact-page spad">
		<div class="container">
            @include('frontend\layout\_alert')
			<div class="row">
				<div class="col-lg-6">
                    <h3 class="text-center mb-4">Register</h3>
					<form class="contact-form" method="POST" action="{{ route('user.create')}}">
                        @csrf
						<div class="row">
							<div class="col-md-12">
								<div class="form-group">
									<input class="check-form" type="text" name="firstname" placeholder="First Name" required value="{{old('firstname')}}">
                                    <span><i class="ti-check"></i></span>
                                    @error('firstname')
                                        <lable class="text-danger text-center">{{$message}}</lable>
                                    @enderror

								</div>
							</div>

							<div class="col-md-12">
								<div class="form-group">
									<input class="check-form" type="text" name="lastname" placeholder="Last Name" required value="{{old('lastname')}}">
                                    <span><i class="ti-check"></i></span>
                                    @error('lastname')
                                        <lable class="text-danger text-center">{{$message}}</lable>
                                    @enderror

								</div>
							</div>

							<div class="col-md-12">
								<div class="form-group">
									<input class="check-form" type="email" name="email" placeholder="Email Address" required value="{{old('email')}}">
                                    <span><i class="ti-check"></i></span>
                                    @error('email')
                                        <lable class="text-danger text-center">{{$message}}</lable>
                                    @enderror

								</div>
							</div>
							<div class="form-group col-md-12">
                                <label for="exampleFormControlSelect1">Gender</label>
                                <select class="form-control" id="exampleFormControlSelect1" name="gender">
                                    <option value="" selected="selected">- Select -</option>
                                  <option value="male">Male</option>
                                  <option value="female">Female</option>
                                </select>
                                @error('gender')
                                        <lable class="text-danger text-center">{{$message}}</lable>
                                    @enderror
                            </div>
							<div class="form-group col-md-12">
                                <label for="exampleFormControlSelect1">State</label>
                                <select class="form-control" id="exampleFormControlSelect1" name="state">
                                    <option value="" selected="selected">- Select -</option>
                                    <option value="Abuja">Abuja FCT</option>
                                    <option value="Abia">Abia</option>
                                    <option value="Adamawa">Adamawa</option>
                                    <option value="Akwa Ibom">Akwa Ibom</option>
                                    <option value="Anambra">Anambra</option>
                                    <option value="Bauchi">Bauchi</option>
                                    <option value="Bayelsa">Bayelsa</option>
                                    <option value="Benue">Benue</option>
                                    <option value="Borno">Borno</option>
                                    <option value="Cross River">Cross River</option>
                                    <option value="Delta">Delta</option>
                                    <option value="Ebonyi">Ebonyi</option>
                                    <option value="Edo">Edo</option>
                                    <option value="Ekiti">Ekiti</option>
                                    <option value="Enugu">Enugu</option>
                                    <option value="Gombe">Gombe</option>
                                    <option value="Imo">Imo</option>
                                    <option value="Jigawa">Jigawa</option>
                                    <option value="Kaduna">Kaduna</option>
                                    <option value="Kano">Kano</option>
                                    <option value="Katsina">Katsina</option>
                                    <option value="Kebbi">Kebbi</option>
                                    <option value="Kogi">Kogi</option>
                                    <option value="Kwara">Kwara</option>
                                    <option value="Lagos">Lagos</option>
                                    <option value="Nassarawa">Nassarawa</option>
                                    <option value="Niger">Niger</option>
                                    <option value="Ogun">Ogun</option>
                                    <option value="Ondo">Ondo</option>
                                    <option value="Osun">Osun</option>
                                    <option value="Oyo">Oyo</option>
                                    <option value="Plateau">Plateau</option>
                                    <option value="Rivers">Rivers</option>
                                    <option value="Sokoto">Sokoto</option>
                                    <option value="Taraba">Taraba</option>
                                    <option value="Yobe">Yobe</option>
                                    <option value="Zamfara">Zamfara</option>
                                    <option value="Outside Nigeria">Outside Nigeria</option>
                                </select>
                                @error('state')
                                        <lable class="text-danger text-center">{{$message}}</lable>
                                    @enderror
                            </div>
                            <div class="form-group col-md-12">
                                <label for="exampleFormControlSelect1">Social Status</label>
                                <select class="form-control" id="exampleFormControlSelect1" name="social_status">
                                    <option value="" selected="selected">- Select -</option>
                                    <option value="Undergraduate">Undergraduate</option>
                                  <option value="nysc">NYSC</option>
                                  <option value="employed">Employed</option>
                                  <option value="young-professional">Young Professional</option>
                                  <option value="others">Others</option>
                                </select>
                                @error('social-status')
                                        <lable class="text-danger text-center">{{$message}}</lable>
                                    @enderror
                            </div>
							<div class="col-md-12">
								<div class="form-group">
                                    <button class="site-btn sb-gradients mt-4">Take Action</button>
								</div>
							</div>
						</div>
					</form>
				</div>
				<div class="col-md-6">
                    <h3 class="text-center mb-4">The Pledge</h3>
                    <p>I pledge to Nigeria my country To join the fight to eradicate Coronavirus from Nigeria And to take seriously all safety measures against the virus I take responsibility for protecting myself and others by pledging to</p>
                    <ul class="pledge-list">
                        <li>Wash my hands regularly with soap and water,</li>
                        <li>Practice social distancing and avoid large gatherings,</li>
                        <li>Stay at home and avoid unnecessary outings.</li>
                    </ul>
                    <p class="mt-5">I pledge to Nigeria my country</p>
                    <p>That I will self-isolate and report to the necessary authorities if I show any COVID-19 related symptoms
                    </p>
                    <p>I pledge not to join in spreading unconfirmed information through social media or verbally, that may misinform and increase panic</p>
                    <p>I will also be supportive to those in need at this challenging time</p>
                    <p>
                        <strong>We shall eradicate Coronavirus from Nigeria in record time, so help us
                            God!
                        </strong>
                    </p>                </div>
			</div>
		</div>
	</section>
	<!-- Contact section end -->


	<!-- Newsletter section -->
	<section class="newsletter-section gradient-bg">
		<div class="container text-white text-center">
			<h2>Youth Against Coronavirus</h2>
		</div>
	</section>
	<!-- Newsletter section end -->
@endsection
