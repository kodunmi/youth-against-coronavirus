@extends('frontend.layout._master')
@section('content')
<!-- Page info section -->
	<section class="page-info-section">
		<div class="container">
			<h2>Activities</h2>
			<div class="site-beradcamb">
				<a href="{{ route('home')}}">Home</a>
				<span><i class="fa fa-angle-right"></i> Activities</span>
			</div>
		</div>
	</section>
	<!-- Page info end -->



	<!-- Contact section -->
	<section class="contact-page spad">
		<div class="container">
            <h3 class="text-center mb-4">Proposed Activities</h3>
                <div class="col-md-8 offset-md-2">
                    <ul class="list-group list-group-flush text-center">
                        <li class="list-group-item">Produce and disseminate youth centric information about Coronavirus through social media channels.</li>
                        <li class="list-group-item">Promote the Youth Pledge Against Coronavirus.</li>
                        <li class="list-group-item">Present a Youth Position Paper Against Coronavirus to the government at different levels.</li>
                        <li class="list-group-item">Activate a Youth Toll Free Line on Coronavirus.</li>
                        <li class="list-group-item">Encourage youth initiatives towards eradicating Coronavirus from Nigeria.</li>
                    </ul>
                </div>
			</div>
		</div>
	</section>
	<!-- Contact section end -->


	<!-- Newsletter section -->
	<section class="newsletter-section gradient-bg">
		<div class="container text-white">
			<div class="row">
				<div class="col-lg-7 newsletter-text">
					<h2>Subscribe to our Newsletter</h2>
					<p>Sign up for our weekly industry updates, insider perspectives and in-depth market analysis.</p>
				</div>
				<div class="col-lg-5 col-md-8 offset-lg-0 offset-md-2">
					<form class="newsletter-form">
						<input type="text" placeholder="Enter your email">
						<button>Get Started</button>
					</form>
				</div>
			</div>
		</div>
	</section>
	<!-- Newsletter section end -->
@endsection
