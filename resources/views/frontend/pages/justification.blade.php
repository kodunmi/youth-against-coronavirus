@extends('frontend.layout._master')
@section('content')
<!-- Page info section -->
	<section class="page-info-section">
		<div class="container">
			<h2>Justification</h2>
			<div class="site-beradcamb">
				<a href="{{ route('home')}}">Home</a>
				<span><i class="fa fa-angle-right"></i> Justification</span>
			</div>
		</div>
	</section>
	<!-- Page info end -->



	<!-- Contact section -->
	<section class="contact-page spad">
		<div class="container">
            <h3 class="text-center mb-4">Justification</h3>
			<div class="col-md-8 offset-md-2 text-justify">
                <p>The novel deadly COVID-19 pandemic virus has entered Nigeria and threatens our national existence if allowed to spread at the exponential rate being recorded in several countries of the world.</p>
                <p>On February 27th, 2020, laboratory tests confirmed Nigeria’s first case of the deadly virus. However, between Friday 13th and 19th, March 2020, Nigeria’s cases increased from 3 to 12 cases with 1 death. Meanwhile, Lagos state is still tracing about 1,300 persons for further testing and monitoring.</p>
                <p>The concern is that Nigeria does not have the required health care systems, adequate personnel and basic infrastructure to contain an exponential spread of COVID-19. Even in developed countries with far better health care system, personnel and infrastructure, COVID-19 is overwhelming them and leading to total shutdown of states and communities. </p>
                <p>Based on statistics, over 60% of Nigeria’s population are youth and therefore would be the most affected by the negative impact of COVID-19 if it is not eradicated. It is therefore very imperative for the youth community to urgently get involved in the fight against COVID-19 at all levels. </p>
                <p>The world is already being negatively impacted by COVID-19 and Nigeria is not being left out. For instance, Nigeria has announced travel ban on 13 countries with over 1,000 cases of the virus. Events are being cancelled, schools are being shut down, public gatherings have been restricted to smaller numbers, airports are on lock-down, imports and exports across countries have been halted and affecting local trade, oil prices have dropped significantly. Nigeria’s 2020 budget has been cut down by N1.5 trillion, government has suspended recruitment, the economy is already experiencing distress, everyone is being affected by the impact of COVID-19 and as such, the youth must also rise, organize and participate in efforts against the virus.</p>
            </div>
		</div>
	</section>
	<!-- Contact section end -->


	<!-- Newsletter section -->
	<section class="newsletter-section gradient-bg">
		<div class="container text-white">
			<div class="row">
				<div class="col-lg-7 newsletter-text">
					<h2>Subscribe to our Newsletter</h2>
					<p>Sign up for our weekly industry updates, insider perspectives and in-depth market analysis.</p>
				</div>
				<div class="col-lg-5 col-md-8 offset-lg-0 offset-md-2">
					<form class="newsletter-form">
						<input type="text" placeholder="Enter your email">
						<button>Get Started</button>
					</form>
				</div>
			</div>
		</div>
	</section>
	<!-- Newsletter section end -->
@endsection
