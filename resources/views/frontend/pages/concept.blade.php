@extends('frontend.layout._master') @section('content')
<!-- Page info section -->
<section class="page-info-section">
    <div class="container">
        <h2>About</h2>
        <div class="site-beradcamb">
            <a href="{{ route('home')}}">Home</a>
            <span><i class="fa fa-angle-right"></i> About</span>
        </div>
    </div>
</section>
<!-- Page info end -->



<!-- Contact section -->
<section class="contact-page spad">
    <div class="container">
        <div class="row">
            <h3 class="text-center mb-4 col-lg-12">About YAC</h3>
            <div class="col-md-8 offset-md-2">
                <p>‘Youth Against Coronavirus’ is a coalition designed to galvanize coordinated youth response and participation in national efforts to manage and halt further spread of the novel Coronavirus (COVID-19) pandemic in Nigeria. The coalition
                    is to ensure that Nigerian youths, NGOs and young professionals are mobilized as stakeholders to actively contribute knowledge, expertise and resources to support governmental efforts at eradicating the virus both at national and state
                    government levels in a coordinated and measurable way. The coalition shall be coordinated at the national and state levels through volunteer coordinators. It shall leverage social media, traditional media channels and influencers to
                    drive engagement and collaboration in ensuring that all hands are on deck to halt and eradicate COVID-19 from Nigeria.</p>
                <a href="{{ route('register')}}" class="site-btn sb-gradients mt-5">Pledge</a>
            </div>
            <h3 class="text-center mb-4 mt-5 col-lg-12" id="obj">Objective</h3>
            <div class="col-md-8 offset-md-2">
                <!-- feature -->
                <div class="feature" id="feature">
                    <i class="ti-support"></i>
                    <div class="feature-content">
                        <h4>Facilitation</h4>
                        <p>Facilitate youth ideas, expertise and resources towards eradicating Coronavirus in Nigeria.
                        </p>
                    </div>
                </div>
                <!-- feature -->
                <div class="feature">
                    <i class="ti-announcement"></i>
                    <div class="feature-content">
                        <h4>Awareness</h4>
                        <p>Frequently provide youth friendly awareness creation and information sharing about COVID-19.
                        </p>
                    </div>
                </div>
                <!-- feature -->
                <div class="feature">
                    <i class="ti-headphone-alt"></i>
                    <div class="feature-content">
                        <h4>Advocacy</h4>
                        <p>Advocate for proactive, efficient and inclusive management of COVID-19 pandemic by government at all levels. </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- Contact section end -->


<!-- Newsletter section -->
<section class="newsletter-section gradient-bg">
    <div class="container text-white">
        <div class="row">
            <div class="col-lg-7 newsletter-text">
                <h2>Subscribe to our Newsletter</h2>
                <p>Sign up for our weekly industry updates, insider perspectives and in-depth market analysis.</p>
            </div>
            <div class="col-lg-5 col-md-8 offset-lg-0 offset-md-2">
                <form class="newsletter-form">
                    <input type="text" placeholder="Enter your email">
                    <button>Get Started</button>
                </form>
            </div>
        </div>
    </div>
</section>
<!-- Newsletter section end -->
@endsection