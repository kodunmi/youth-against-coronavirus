@if (Session::has('message'))
<div class="alert  alert-{{Session::get('type')}}" role="alert">
    {{ Session::get('message') }}
    <a href="/#action" class="btn btn-primary">click here to take action</a>
</div>
@endif
