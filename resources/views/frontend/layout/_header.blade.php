<header class="header-section clearfix">
    <div class="container-fluid">
        <a href="/" class="site-logo">
            <img src="/frontend/img/logo.png" alt="">
        </a>
        <div class="responsive-bar"><i class="fa fa-bars"></i></div>
        <a href="" class="user"><i class="fa fa-user"></i></a>
            <a href="{{ route('register')}}" class="site-btn">Take Action</a>


        <nav class="main-menu">
            <ul class="menu-list">
                <li><a href="/#pledge">Pledge</a></li>
                <li><a href="/concept#obj">Objectives</a></li>
                <li><a href="#">Resources</a></li>
                <li><a href="#">Reports</a></li>
                <li><a href="{{ route('contact')}}">Contact</a></li>
                <a href="{{ route('register')}}" class="btn btn-primary btn-block text-white mb-4 nav-btn">Take Action</a>
            </ul>
        </nav>
    </div>
</header>
